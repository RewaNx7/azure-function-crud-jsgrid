## What is this Project for? ##

### Azure-function-CRUD-jsgrid Project which implemented by using JSGrid, it is a lightweight client-side data grid       control based on jQuery.
### It supports basic grid operations like inserting, filtering, editing, deleting, paging, and sorting.
### You can check its documentation from here: http://js-grid.com/.

## How do i get set up? ##

### Download the project, and open index.html in your browser.
### 1. you can change the Web page title from 'Line 8' to adjust it by your own. [Optional]

### 2. you can change the Web page font from 'Line 11' to any font you like, Simply you can select any font you      want from this link: 'https://fonts.google.com/' after selecting this font, pleae place its link instead of the       used link in 'Line 11'. Don't forget to change the Font-Family to the new Font on lines: 37, 47, 54, 75, 82. [Optional]

### 3. you can change the 'h1' of the Web page on 'Line 92'. [Optional]

### 4. you can change the API_URL on 'Line 104'. [Required]

### 5. related to Grid Table, you can change the Confirmation Message while deleting any table item on 'Line 268'. [Optional]

### 6. If you wanna add more fields, you can add a JSON object on 'Line 344'. [Optional]



